# Pygame

Presented to Python Working Group at WSU Pullman 2019, 2020. Simple progression of files to demo fundamentals of pygame, but no user interrupts yet. Looped animation only.

The image files are pixel art found online, not my own.