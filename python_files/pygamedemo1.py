#!/usr/bin/env python2

import pygame as pg
from pygame.locals import *

#https://dr0id.bitbucket.io/legacy/pygame_tutorial00.html
#https://inventwithpython.com/pygame
#https://nerdparadise.com/programming/pygame

def main():
    ### initialize the pygame module
    pg.init()
    
     
    ### create a surface on screen that has the size of 240 x 180
    #screen = pg.display.set_mode((240,180))

    #screen = pg.display.set_mode((800,800))

    ### set up the window

    DISPLAYSURF = pg.display.set_mode((600, 600), 0, 32)
    

    ### set up the colors
    BLACK = (  0,   0,   0)
    WHITE = (255, 255, 255)
    RED = (255,   0,   0)
    GREEN = (  0, 255,   0)
    BLUE = (  0,   0, 255)
    AQUA = (  0, 128, 128)

    ### draw on the surface object
    
    #pg.draw.polygon()
    #pg.draw.circle()
    pg.draw.polygon(DISPLAYSURF, GREEN, ((146, 0), (291, 106), (236, 277), (56, 277), (0, 106)))
    pg.draw.line(DISPLAYSURF, BLUE, (60, 60), (120, 60), 4)
    pg.draw.line(DISPLAYSURF, BLUE, (250,20), (250,460), 7)
    pg.draw.line(DISPLAYSURF, BLUE, (60, 120), (120, 420), 12)
    pg.draw.circle(DISPLAYSURF, AQUA, (400, 150), 40, 0)
    pg.draw.ellipse(DISPLAYSURF, RED, (300, 250, 40, 80), 3)
    pg.draw.rect(DISPLAYSURF, RED, (200, 150, 100, 50))
    

     
    ### define a variable to control the main loop
    running = True
     
    # main loop
    while running:
        # event handling, gets all events from the event queue
        for event in pg.event.get():
            # only do something if the event is of type QUIT
            if event.type == pg.QUIT:
                # change the value to False, to exit the main loop
                running = False
        pg.display.update()
     
# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__=="__main__":
    # call the main function
    main()
