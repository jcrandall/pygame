#!/usr/bin/env python2

import pygame as pg
from pygame.locals import *

#https://dr0id.bitbucket.io/legacy/pygame_tutorial00.html
#https://inventwithpython.com/pygame
#https://nerdparadise.com/programming/pygame

def main():
    # initialize the pygame module
    pg.init()
    

    # set up the window

    DISPLAYSURF = pg.display.set_mode((600, 600), 0, 32)   ###args: width and height, style of header, bits per pixel
    pg.display.set_caption('An Awesome Demo!')

    # set up the colors
    BLACK = (  0,   0,   0)
    WHITE = (255, 255, 255)
    RED = (255,   0,   0)
    GREEN = (  0, 255,   0)
    BLUE = (  0,   0, 255)
    AQUA = (  0, 128, 128)

    # color the background of the surface object
    DISPLAYSURF.fill(WHITE)
    

    pixObj = pg.PixelArray(DISPLAYSURF)
    pixObj[480][380] = BLACK
    pixObj[482][382] = BLACK
    pixObj[484][384] = BLACK
    pixObj[486][386] = BLACK
    pixObj[488][388] = BLACK
    
    pxarray = pg.PixelArray(DISPLAYSURF)
    #pxarray[0:25, 0:45] = BLACK
    pxarray[::5, ::5] = BLACK

    ### more of our favorite shapes
    pg.draw.ellipse(DISPLAYSURF, RED, (300, 250, 40, 80), 3)
    pg.draw.rect(DISPLAYSURF, RED, (200, 150, 100, 50))
    pg.draw.polygon(DISPLAYSURF, GREEN, ((146, 0), (291, 106), (236, 277), (56, 277), (0, 106)))
    
    del pixObj   ##this will "unlock" the surface object so we can draw on it again
     
    # define a variable to control the main loop
    running = True
     
    # main loop
    while running:
        # event handling, gets all event from the event queue
        for event in pg.event.get():
            # only do something if the event is of type QUIT
            if event.type == pg.QUIT:
                # change the value to False, to exit the main loop
                running = False
        pg.display.update()
     
     
# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__=="__main__":
    # call the main function
    main()
