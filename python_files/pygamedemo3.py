#!/usr/bin/env python2
import pygame as pg

from pygame.locals import *

pg.init()

FPS = 10 # frames per second setting
fpsClock = pg.time.Clock()

# set up the window
#DISPLAYSURF = pg.display.set_mode((800,800))
DISPLAYSURF = pg.display.set_mode((800, 800), 0, 32)
pg.display.set_caption('Random Plumber')

WHITE = (255, 255, 255)
BLACK = (  0,   0,   0)
RED = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE = (  0,   0, 255)

myImg = pg.image.load('mariotiny.png').convert()
smallImg = pg.transform.scale(myImg, (40,40))

#font = pg.font.SysFont()
font = pg.font.SysFont("comicsansms",60)     ##where 60 is the font size
msg = font.render("Wow!",True, BLACK )  


myx = 10
myy = 10
direction = 'right'



while True: # the main game loop

    DISPLAYSURF.fill(GREEN)

    if direction == 'right':

        myx += 5
        if myx == 280:

            direction = 'down'

    elif direction == 'down':

        myy += 5
        if myy == 220:

            direction = 'left'

    elif direction == 'left':
        
        myx -= 5
        
        if myx == 10:

            direction = 'up'

    elif direction == 'up':
        DISPLAYSURF.blit(msg, (50,50))
        myy -= 5

        if myy == 10:

            direction = 'right'
    

    ###the .blit() method is what makes the image appear   
    pg.draw.rect(DISPLAYSURF, RED, (200, 150, 100, 50))
    DISPLAYSURF.blit(smallImg, (myx, myy))
    
    

    for event in pg.event.get():

        if event.type == QUIT:

            pg.quit()

            #sys.exit()

    pg.display.update()
    

    fpsClock.tick(FPS)