#!/usr/bin/env python2
import pygame as pg

from pygame.locals import *

pg.init()

FPS = 40 # frames per second setting
fpsClock = pg.time.Clock()

# some color choices
WHITE = (255, 255, 255)
BLACK = (  0,   0,   0)
RED = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE = (  0,   0, 255)


# set up the window
DISPLAYSURF = pg.display.set_mode((800, 800), 0, 32)
pg.display.set_caption('Random Plumber vs The Cavecrafters')


myImg = pg.image.load('mariotiny.png').convert()
smallImg = pg.transform.scale(myImg, (40,40))
thesword = pg.image.load('swordtiny.png')
smallsword = pg.transform.scale(thesword, (80,80))


#font = pg.font.SysFont()
font = pg.font.SysFont("comicsansms",60)     ##where 60 is the font size
msg1 = font.render("Here we go!",True, BLACK )
msg2 = font.render("Attack!!",True, BLUE )
msg3 = font.render("Run away!!",True, BLACK )
msg4 = font.render("Back home...",True, BLACK )  


theswordx = 400
theswordy = 400
myx = 10
myy = 10
direction = 'right'


while True: # the main game loop
    DISPLAYSURF.fill(WHITE)
    pxarray = pg.PixelArray(DISPLAYSURF)
    pxarray[::5, ::5] = GREEN
    del pxarray
    

    if direction == 'right':
        DISPLAYSURF.blit(msg1, (50,50))    ###the .blit() method is what makes the item appear
        myx += 5
        for i in range(1, myx, 20):
            ####try to leave a trail behind the plumber
            pg.draw.rect(DISPLAYSURF, BLUE, pg.Rect(i,myy,10,10))

        if myx == 280:

            direction = 'down'

    elif direction == 'down':
        DISPLAYSURF.blit(msg2, (450,300))
        myy += 5
        theswordx -= 2
        theswordy -= 2
        for i in range(1, myy, 20):
            pg.draw.rect(DISPLAYSURF, BLUE, pg.Rect(myx,i,10,10))
            
            
        if myy == 220:

            direction = 'left'

    elif direction == 'left':
        DISPLAYSURF.blit(msg3, (50,300))
        theswordx += 2
        theswordy += 2
        myx -= 5
        if myx == 10:

            direction = 'up'

    elif direction == 'up':
        DISPLAYSURF.blit(msg4, (50,50))
        myy -= 5

        if myy == 10:

            direction = 'right'
    
    

    ###the .blit() method is what makes the image appear   
    
    DISPLAYSURF.blit(smallsword, (theswordx, theswordy))
    DISPLAYSURF.blit(smallImg, (myx, myy))
    pg.draw.rect(DISPLAYSURF, RED, (100, 450, 200, 50))
    pg.draw.rect(DISPLAYSURF, RED, (350, 50, 150, 50))
    pg.draw.rect(DISPLAYSURF, RED, (250, 550, 400, 50))
    pg.draw.rect(DISPLAYSURF, RED, (200, 150, 100, 50))

    for event in pg.event.get():

        if event.type == QUIT:

            pg.quit()

            #sys.exit()

    pg.display.update()
    

    fpsClock.tick(FPS)